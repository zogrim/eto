all: clean
	echo d | xcopy etho-dogdatabase.elte.hu D:\progs\wamp\www\eto\etho-dogdatabase.elte.hu /s /e
	echo d | xcopy protectedData D:\progs\wamp\www\eto\protectedData /s /e

clean: cleanfar
	del filelist
	del messages.*
	del /s *~

cleanfar:
	rmdir /S /Q D:\progs\wamp\www\eto\etho-dogdatabase.elte.hu
	rmdir /S /Q D:\progs\wamp\www\eto\protectedData

dict: flist
	xgettext -n -f filelist

flist:
	dir *.php /L /B /S > filelist

trans:
	msgfmt messages.po
