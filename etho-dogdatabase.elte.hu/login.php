<?php

require_once '../protectedData/config.php';

require_once PROT_ROOT . 'session.php';
require_once PROT_ROOT . 'ui_data.php';
require_once PROT_ROOT . 'get.php';
require_once PROT_ROOT . 'post.php';
require_once PROT_ROOT . 'functions.php';
require_once PROT_ROOT . 'ar/ActiveRecord.php';
require_once PROT_ROOT . 'pw/lib/password.php';

$sesh = Session::getInstance();
$post = Post::getInstance();
$get = Get::getInstance();
$data = new UIData;

setLocalization('en');

// start ActiveRecord db connection
initAR();

// TODO session security revamp
if (isset($sesh->uid))
{
    // if logged in send to index
    header('Location: index.php');
    exit;
}

if ($post->isRequest())
{
    //--------------------------------------------------
    //    POST submitLogin
    //--------------------------------------------------
    // login attempt with email -> ask for auth with password
    if ($post->isForm('submitLogin'))
    {
	// email required
	$data->checkPresence($post, 'email', _('login_missing_email'));
	if ($data->isError())
	{
	    //if missing, back to login
	    includeView('login.view.php', $data);
	} else {
	    // otherwise go to authenticate
	    $data->email = $post->email;
	    includeView('auth.view.php', $data);
	}
	//--------------------------------------------------
	//    POST submitAuth
	//--------------------------------------------------
	// authenticate with password
    } else if ($post->isForm('submitAuth')) {
	// email required
	$data->checkPresence($post, 'email', _('login_missing_email'));
	// password required
	$data->checkPresence($post, 'password', _('login_missing_password'));
	// if no error so far, fetch user data
	if (!$data->isError)
	{
	    $data->email = $post->email;
	    $user = User::find_by_email($post->email);
	    if (!isset($user))
	    {
		$data->error( _('login_bad_credentials') );
	    } else {
		// TODO password hash verification
		if ($user->pw_hash === $post->password)
		{
		    // TODO session security revamp
		    $sesh->uid = $user->id;
		    $sesh->type = $user->type;
		    header('Location: index.php');
		    exit;
		} else {
		    $data->error( _('login_bad_credentials') );
		}
	    }
	}
	// there was an error during login
	// otherwise this should never run
	includeView('auth.view.php', $data);
    }
} else {
    $data->email = (isset($get->email)) ? $get->email : '';

    //--------------------------------------------------
    //    GET login.php?site=auth
    //--------------------------------------------------
    // if redirected to authenticate site, then follow
    if ($get->site == 'auth')
    {
	includeView('auth.view.php', $data);	
    } else {
	includeView('login.view.php', $data);	
    }
}


?>

