<?php

require_once '../protectedData/config.php';

require_once PROT_ROOT . 'session.php';
require_once PROT_ROOT . 'ui_data.php';
require_once PROT_ROOT . 'post.php';
require_once PROT_ROOT . 'get.php';
require_once PROT_ROOT . 'functions.php';
require_once PROT_ROOT . 'ar/ActiveRecord.php';
require_once PROT_ROOT . 'pw/lib/password.php';

$sesh = Session::getInstance();
$post = Post::getInstance();
$get = Get::getInstance();
$data = new UIData;

setLocalization('en');

// start ActiveRecord db connection
initAR();

// TODO session security revamp
if (!isset($sesh->uid) || $sesh->type != 'A')
{
    // not logged in and non admins redirect to index
    header("Location: index.php");
    exit;
}

if ($post->isRequest())
{
    //--------------------------------------------------
    //    POST submitAddBreed
    //--------------------------------------------------
    // attempt to add new breed
    if ($post->isForm('submitAddBreed'))
    {
	// breed contains locale specific data
	$data->locales = $localizations;
	// check required fields
	$data->checkPresence($post, 'name_id', _('admin_breed_id_missing'));
	foreach($localizations as $loc => $text)
	{
	    $data->checkPresence($post, $loc . '_text', _('admin_breed_text_missing'));
	}
	// keep post data for new form
	$data->copyData($post);

	// if no error so far, try saving
	if (!$data->isError())
	{
	    $br = new Breed();
	    $br->name_id = mb_strtoupper($post->name_id, 'utf-8');
	    $br->create_id = $sesh->uid;

	    // check if model is valid
	    if ($br->is_valid())
	    {
		$br->save();
		// for each locale
		foreach ($localizations as $loc => $text)
		{
		    $key = $loc . '_text';
		    // create and save locale data
		    $br->create_breed_text(array('localization' => $loc,
						 'text' => $post->$key));
		}
		header("Location: admin.php?site=breed_list");
	    } else {
		// if invalid, get model errors for display
		$data->errorFromModel($br->errors->get_raw_errors());
	    }
	}

	includeView('admin/add_breed.view.php', $data);
	
	//--------------------------------------------------
	//    POST submitModifyBreed
	//--------------------------------------------------
    } else if ($post->isForm('submitModifyBreed')){
	// breed contains locale specific data
	$data->locales = $localizations;
	// check required fields - sanity check for readonly input
	$data->checkPresence($post, 'name_id', _('admin_breed_id_missing'));
	foreach($localizations as $loc => $text)
	{
	    $data->checkPresence($post, $loc . '_text', _('admin_breed_text_missing'));
	}
	// keep post data for new form
	$data->copyData($post);

	// if no error so far, try update
	if (!$data->isError())
	{
	    $br = Breed::find_by_name_id($post->name_id);
	    // if result found
	    if (isset($br))
	    {
		$br->modify_id = $sesh->uid;
		$br->modified = new ActiveRecord\DateTime();
		
		// check if model is valid
		if ($br->is_valid())
		{
		    $br->save();
		    // for each locale
		    foreach ($localizations as $loc => $text)
		    {
			$key = $loc . '_text';
			// fetch existing text
			$bt = BreedText::find_by_breed_id_and_localization(
			    $br->id, $loc);
			// if already exists
			if (isset($bt))
			{
			    // update text and save
			    $bt->text = $post->$key;
			    $bt->save();
			    //otherwise create new breed text
			} else {
			    $br->create_breed_text(
				array('localization' => $loc,
				      'text' => $post->$key));			
			}
		    }
		    header("Location: admin.php?site=breed_list");
		} else {
		    // if invalid, get model errors for display
		    $data->errorFromModel($br->errors->get_raw_errors());
		}
		// if no result was found for given name_id
	    } else {
		$data->error( _('breed_id_not_found_in_database'));
	    }
	}
	includeView('admin/breed.view.php', $data);
	
	//--------------------------------------------------
	//    POST submitDeleteBreed
	//--------------------------------------------------
    } else if ($post->isForm('submitDeleteBreed')){
	
	
	//--------------------------------------------------
	//    POST unhandled redirect to admin index
	//--------------------------------------------------
    } else {
	header('Location: admin.php');
    }
}

if ($get->isRequest())
{
    // depending on witch site was requested
    switch ($get->site)
    {
	
	//--------------------------------------------------
	//    GET admin.php?site=breed?id=name_id
	//--------------------------------------------------
	case 'breed':
	// breed contains locale specific data
	$data->locales = $localizations;
	// fetch breed by name_id then corresponding texts
	$br = Breed::find_by_name_id($get->id);
	// if no results were found redirect
	if (!isset($br))
	{
	    header('Location: admin.php?site=breed_list');
	    exit;
	}
	$bts = $br->breed_texts;
	// store name_id for ui
	$data->name_id = $br->name_id;
	// for each localization we fetch data from texts
	foreach($localizations as $loc => $text)
	{
	    $key = $loc . '_text';
	    foreach ($bts as $k => $bt)
	    {
		// store current breed text if it is in the actual locale
		if ($bt->localization == $loc)
		{
		    $data->$key = $bt->text;
		}
	    }
	}
	includeView('admin/breed.view.php', $data);
	break;

	//--------------------------------------------------
	//    GET admin.php?site=add_breed
	//--------------------------------------------------
	case 'add_breed':
	// breed contains locale specific data
	$data->locales = $localizations;
	includeView('admin/add_breed.view.php', $data);
	break;
	
	//--------------------------------------------------
	//    GET admin.php?site=breed_list
	//--------------------------------------------------
	case 'breed_list':
	// get the name of all breeds
	$data->copyData(Breed::find('all', array('select' => 'name_id')));
	includeView('admin/breed_list.view.php', $data);
	break;

	//--------------------------------------------------
	//    GET admin.php?site=index
	//--------------------------------------------------
	case 'index':
	// other site attempts redirect to admin index
	default:
	includeView('admin/admin.view.php', $data);
    }
}
?>
