<?php
require_once '../protectedData/config.php';

require_once PROT_ROOT . 'session.php';
require_once PROT_ROOT . 'ui_data.php';
require_once PROT_ROOT . 'post.php';
require_once PROT_ROOT . 'get.php';
require_once PROT_ROOT . 'functions.php';
require_once PROT_ROOT . 'ar/ActiveRecord.php';
require_once PROT_ROOT . 'pw/lib/password.php';

$sesh = Session::getInstance();
$post = Post::getInstance();
$get = Get::getInstance();
$data = new UIData;

setLocalization('en');

// start ActiveRecord db connection
initAR();

// TODO session security revamp
if (isset($sesh->uid))
{
    
    // when logged in
    includeView('index.view.php', $data);
} else {
    // if not logged in, send user to login page
    header('Location: login.php');
    exit;
}

?>
