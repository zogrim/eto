<?php
require_once '../protectedData/config.php';

require_once PROT_ROOT . 'session.php';
require_once PROT_ROOT . 'ui_data.php';
require_once PROT_ROOT . 'post.php';
require_once PROT_ROOT . 'functions.php';
require_once PROT_ROOT . 'ar/ActiveRecord.php';
require_once PROT_ROOT . 'pw/lib/password.php';

$sesh = Session::getInstance();
$post = Post::getInstance();
$data = new UIData;

setLocalization('en');

// start ActiveRecord db connection
initAR();

// TODO session security revamp
if (isset($sesh->uid))
{
    // if logged in send to index
    header('Location: index.php');
    exit;
}

if ($post->isRequest())
{
    //--------------------------------------------------
    //    POST submitToRegister
    //--------------------------------------------------
    // redirected from login.php
    if ($post->isForm('submitToRegister'))
    {
	$user = User::find_by_email($post->email);

	// if user already exits and this is first time login/register attempt
	if (isset($user) && $user->first_time)
	{
	    // if user has no password yet
	    if ($user->pw_hash === null)
	    {
		// TODO generate pw hash and save for user
		// TODO send mail containing temporary password
		// TODO message user about email
		//header("refresh:10; url=login.php?email={$post->email}");
		header("Location: login.php?site=auth&email={$post->email}");
		exit;
	    } else {
		// if password was already generated
		// TODO what to do?
		header("Location: login.php?site=auth&email={$post->email}");
		exit;
	    }
	}

	// new user (or possibly old user tries to re-register)	
	$data->email = $post->email;
	includeView('register.view.php', $data);
	
	//--------------------------------------------------
	//    POST submitRegister
	//--------------------------------------------------
	// attempt to register data
    } else if ($post->isForm('submitRegister')) {
	print_r($_POST);
	// check required fields
	$data->checkPresence($post, 'email', _('login_missing_email'));
	$data->checkPresence($post, 'name', _('login_missing_name'));
	$data->checkPresence($post, 'gender', _('login_missing_gender'));
	$data->checkPresence($post, 'dog_name', _('login_missing_dog_name'));
	$data->checkPresence($post, 'breed', _('login_missing_breed'));
	$data->checkPresence($post, 'pedno', _('login_missing_pedno'));
	$data->checkPresence($post, 'dog_gender', _('login_missing_dog_gender'));
	// neutreason required if neutered checked
	// neutdate required if neutered checked
	if (!$data->isError() &&
	    (isset($post->neutered) && $post->neutered !== '' ))
	{
	    $data->checkPresence($post, 'neutreason', _('login_missing_neutreason'));
	    // TODO year, month, day?
	    $data->checkPresence($post, 'neut_year', _('login_missing_neutdate'));
	}

	// keep post data for new form
	$data->copyData($post);

	// if no error so far, try saving
	
	includeView('register.view.php', $data);
    }
} else {
    includeView('register.view.php', $data);
}


?> 
