<?php

class Session {

    const SESSION_STARTED = TRUE;
    const SESSION_NOT_STARTED = FALSE;

    private $state = self::SESSION_NOT_STARTED;

    private static $instance;

    private function __construct() {}
    
    public static function getInstance()
    {
	if (!isset(self::$instance))
	{
	    self::$instance = new self;
	}
	self::$instance->start();

	return self::$instance;
    }

    public function start()
    {
	if ($this->state == self::SESSION_NOT_STARTED)
	{
	    session_name('sid');
	    $scp = session_get_cookie_params();
	    $scp['httponly'] = true;
	    
	    session_set_cookie_params($scp['lifetime'],
				     $scp['path'],
				     $scp['domain'],
				     $scp['secure'],
				     $scp['httponly']);

	    $this->state = session_start();
	}
	return $this->state;
    }

    public function regenerate()
    {
	
	
    }

    public function __set($key, $value)
    {
	$_SESSION[$key] = $value;
    }

    public function __get($key)
    {
	if (isset($_SESSION[$key]))
	{
	    return $_SESSION[$key];
	}
    }

    public function __isset($key)
    {
	return (isset($_SESSION[$key]));
    }

    public function __unset($key)
    {
	unset($_SESSION[$key]);
    }

    public function destroy()
    {
	if ($this->state == self::SESSION_STARTED)
	{
	    $this->state = !session_destroy();
	    session_unset();

	    return $this->state;
	}

	return FALSE;
    }
    
}

?> 
