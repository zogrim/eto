<?php

require_once '../protectedData/config.php';

// create and manage HTML input tags dynamically
class FormBuilder {

    public static function link($text, $href, $attr)
    {
	$str = "<a href={$href}";
	$str .= FormBuilder::addAttribute($attr, 'download');
	$str .= FormBuilder::addAttribValue($attr, 'target');
	$str .= FormBuilder::addAttribValue($attr, 'type');
	return $str . '>' . _($text) . '</a>';
    }
    
    // returns <label> syntax with specified attributes
    public static function label($text, $attr)
    {
	$str = '<label';
	$str .= FormBuilder::addAttribValue($attr, 'class');
	$str .= FormBuilder::addAttribValue($attr, 'for');
	$str .= FormBuilder::addAttribValue($attr, 'form');
	return $str . '>' . _($text) . '</label>';
    }

    // returns <input> syntax with specified attributes
    public static function input($attr)
    {
	$str = '<input';

	// TODO probelms: color date datetime datetime-local
	// month search tel time week
	// safari does not support url email
	$str .= FormBuilder::addAttribValue($attr, 'type');
	$str .= FormBuilder::addAttribValue($attr, 'class');
	$str .= FormBuilder::addAttribValue($attr, 'name');
	$str .= FormBuilder::addAttribValue($attr, 'id');
	$str .= FormBuilder::addAttribValueLocale($attr, 'value');
	$str .= FormBuilder::addAttribValue($attr, 'accept');
	$str .= FormBuilder::addAttribValueLocale($attr, 'alt');
	$str .= FormBuilder::addAttribValue($attr, 'autocomplete');
	$str .= FormBuilder::addAttribute($attr, 'autofocus');
	$str .= FormBuilder::addAttribute($attr, 'checked');
	$str .= FormBuilder::addAttribute($attr, 'disabled');
	// TODO internet explorer does not support form
	$str .= FormBuilder::addAttribValue($attr, 'form');
	$str .= FormBuilder::addAttribValue($attr, 'formaction');
	$str .= FormBuilder::addAttribValue($attr, 'formenctype');
	$str .= FormBuilder::addAttribValue($attr, 'formmethod');
	// TODO safari does not support formnovalidate
	$str .= FormBuilder::addAttribValue($attr, 'formnovalidate');
	$str .= FormBuilder::addAttribValue($attr, 'formtarget');
	$str .= FormBuilder::addAttribValue($attr, 'height');
	// TODO safari does not support list
	$str .= FormBuilder::addAttribValue($attr, 'list');
	$str .= FormBuilder::addAttribValue($attr, 'max');
	$str .= FormBuilder::addAttribValue($attr, 'maxlength');
	$str .= FormBuilder::addAttribValue($attr, 'min');
	$str .= FormBuilder::addAttribute($attr, 'multiple');
	// TODO safari does not support pattern
	$str .= FormBuilder::addAttribValue($attr, 'pattern');
	$str .= FormBuilder::addAttribValueLocale($attr, 'placeholder');
	$str .= FormBuilder::addAttribute($attr, 'readonly');
	// TODO safari does not support required
	$str .= FormBuilder::addAttribute($attr, 'required');
	$str .= FormBuilder::addAttribValue($attr, 'size');
	$str .= FormBuilder::addAttribValue($attr, 'src');
	$str .= FormBuilder::addAttribValue($attr, 'step');
	$str .= FormBuilder::addAttribValue($attr, 'width');
	$loc = (isset($attr['text']) ? _($attr['text']) : '');
	return $str . '>' . $loc;
    }

    // returns <textarea> syntax with specified attributes
    public static function textarea($text, $attr)
    {
	$str = '<textarea';
	$str .= FormBuilder::addAttribValue($attr, 'class');
	$str .= FormBuilder::addAttribValue($attr, 'name');
	$str .= FormBuilder::addAttribute($attr, 'rows');
	$str .= FormBuilder::addAttribute($attr, 'cols');
	$str .= FormBuilder::addAttribute($attr, 'autofocus');
	$str .= FormBuilder::addAttribute($attr, 'disabled');
	// TODO internet explorer does not support form
	$str .= FormBuilder::addAttribValue($attr, 'form');
	$str .= FormBuilder::addAttribValue($attr, 'maxlength');
	$str .= FormBuilder::addAttribValueLocale($attr, 'placeholder');
	$str .= FormBuilder::addAttribute($attr, 'readonly');
	// TODO safari does not support required
	$str .= FormBuilder::addAttribute($attr, 'required');
	$str .= FormBuilder::addAttribute($attr, 'wrap');
	return $str . '>' . ($text === '' ? '' : _($text)) . '</textarea>';
    }

    // reads a JSON file that contains the form specification
    // returns a single array of read data
    public static function getUIConfig($file = 'uiconfig.json')
    {
	$ui_json = file_get_contents(PROT_ROOT . $file);
	return json_decode($ui_json, true);
    }

    // assigns form post data to database model
    // $data: instance of ActiveRecord model class
    // $post: instance of Post class containing POST data
    // $mainForm, $subForm: which form's data to be assigned
    // $uiconfig: specification of the form containing data
    public static function assignPostInput($data,
					   $post,
					   $mainForm,
					   $subForm,
					   $uiconfig)
    {
	foreach($uiconfig[$mainForm][$subForm] as $element)
	{
	    // for each <input> tag with possible data field (not buttons)
	    if ($element['method'] == 'input' && $element['type'] != 'submit')
	    {
		// the <input name="name"> points to a column in the database
		// table of the specific model
		// if the value is given assign it, otherwise keep null
		$data->$element['name'] = isset($post->$element['name'])
		    ? $post->$element['name'] : NULL;
	    }
	}
	return $data;
    }

    // creates data array from database model data
    // $model: instance of ActiveRecord model class
    // $mainForm, $subForm: which form's data to be assigned
    // $uiconfig: specification of the form containing data
    public static function createModelData($model,
					   $mainForm,
					   $subForm,
					   $uiconfig)
    {
	$data = array();
	foreach($uiconfig[$mainForm][$subForm] as $element)
	{
	    // for each <input> tag with possible data field (not buttons)
	    if ($element['method'] == 'input' && $element['type'] != 'submit')
	    {
		// the <input name="name"> points to a column in the database
		// table of the specific model
		// if the value is given assign it, otherwise keep null
		$data[$element['name']] = isset($model->$element['name'])
		    ? $model->$element['name'] : '';
	    }
	}
	return $data;
    }

    public static function updateVariable($attr, $key, $data)
    {

	if (isset($attr[$key]) && isset($data[$attr[$key]]))
	{
	    $attr[$key] = $data[$attr[$key]];
	}
	return $attr;
    }

    // adds attribute
    private static function addAttribute($attr, $key)
    {
	if (isset($attr[$key]) && $attr[$key] === true)
	{
	    return " {$key}";
	}
	return "";
    }

    // adds attribute with some kind of value
    private static function addAttribValue($attr, $key)
    {
	if (isset($attr[$key]))
	{
	    return " {$key}=\"{$attr[$key]}\"";
	}
	return "";
    }

    // adds attribute with localized text
    private static function addAttribValueLocale($attr, $key)
    {
	if (isset($attr[$key]))
	{
	    $loc = ($attr[$key] === '' ? '' : _($attr[$key]));
	    return " {$key}=\"{$loc}\"";
	}
	return "";
    }
}

?>

