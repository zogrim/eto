<?php

require_once '../protectedData/config.php';

function initAR()
{
    $isCaesar = function_exists('getCaesarDbConnectionString');
    $devConn = 'mysql://root:enter@localhost/p_dogdatabase';
    $prodConn = ($isCaesar) ? getCaesarDbConnectionString() : $devConn;
    $devConn .= '?charset=utf8';
    $prodConn .= '?charset=utf8';

    $conn = array('development' => $devConn, 'production' => $prodConn);
    $defaultConn = ($isCaesar) ? 'production' : 'development';
    
    ActiveRecord\Config::initialize(function($cfg) use ($conn, $defaultConn)
	{
	    $cfg->set_model_directory(PROT_ROOT . 'models');
	    $cfg->set_connections($conn);
	    $cfg->set_default_connection($defaultConn);
	});
}

function setLocalization($language)
{
    putenv("LANG=$language");
    setlocale(LC_ALL, $language);

    $domain = 'messages';
    bindtextdomain($domain, PROT_ROOT . 'locale');
    textdomain($domain);
}

function startsWith($haystack, $needle)
{
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle)
{
    $len = strlen($haystack) - strlen($needle);
    return $needle === "" || (len >= 0 && strpos($haystack, $needle, $len) !== false);
}

function includeView($path, $data)
{
    $path = PROT_ROOT . 'views/' .$path;
    include PROT_ROOT . 'views/layout.php';
}

?>
