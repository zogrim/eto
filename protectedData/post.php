<?php

class Post implements IteratorAggregate {
    private static $instance;

    private function __construct() {}

    public static function getInstance()
    {
	if (!isset(self::$instance))
	{
	    self::$instance = new self;
	}
	return self::$instance;
    }

    public function __get($key)
    {
	if (isset($_POST[$key]))
	{
	    return htmlspecialchars($_POST[$key]);
	}
	return NULL;
    }
    
    public function __isset($key)
    {
	return (isset($_POST[$key]));
    }

    public function isRequest()
    {
	return ($_SERVER['REQUEST_METHOD'] === 'POST');
    }

    public function isForm($form)
    {
	return (isset($_POST[$form]));
    }

    public function getIterator()
    {
	return new ArrayIterator($_POST);
    }
}

?>
