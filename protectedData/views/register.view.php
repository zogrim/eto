<form action="" method="post" accept-charset="utf-8">
    <h2><?= _('register_human') ?></h2>
    
    <label for="email"><?= _('register_email_label'); ?></label>
    <input type="email" name="email" id="email" value="<?= $data->email ?>"><br>
    <label for="name"><?= _('register_name_label'); ?></label>
    <input type="text" name="name" id="name" value="<?= $data->name ?>"><br>
    <label for="telnum"><?= _('register_telnum_label'); ?></label>
    <input type="text" name="telnum" id="telnum" value="<?= $data->telnum ?>"><br>
    <input type="radio" name="gender" value="M" id="gender_male" <?= ($data->gender == 'M') ? 'checked' : '' ?>>
    <label for="gender_male"><?= _('human_gender_male'); ?></label>
    <input type="radio" name="gender" value="F" id="gender_female" <?= ($data->gender == 'F') ? 'checked' : '' ?>>
    <label for="gender_female"><?= _('human_gender_female'); ?></label><br>
    <label for="birth_year"><?= _('register_birthday_label'); ?></label>
    <input type="number" name="birth_year" id="birth_year" value="<?= $data->birth_year ?>">
    <input type="number" name="birth_month" id="birth_month" value="<?= $data->birth_month ?>">
    <input type="number" name="birth_day" id="birth_day" value="<?= $data->birth_day ?>"><br>
    <label for="address"><?= _('register_address_label'); ?></label>
    <input type="text" name="address" id="address" value="<?= $data->address ?>"><br>
    <label for="qualification"><?= _('register_qualification_label'); ?></label>
    <input type="text" name="qualification" id="qualification" value="<?= $data->qualification ?>"><br>
    <label for="occupation"><?= _('register_occupation_label'); ?></label>
    <input type="text" name="occupation" id="occupation" value="<?= $data->occupation ?>"><br>
    <label for="dognum"><?= _('register_dognum_label'); ?></label>
    <input type="number" name="dognum" id="dognum" value="<?= $data->dognum ?>"><br>
    <label for="comment"><?= _('register_comment_label'); ?></label>
    <textarea name="comment" id="comment"><?= ($data->comment != '') ? $data->comment : _('register_comment_text'); ?></textarea><br>
    <label for="dnd"><?= _('register_dnd_label'); ?></label>
    <input type="checkbox" name="dnd" id="dnd" value="true" <?= ($data->dnd == 'true') ? 'checked' : '' ?>>

    <h2><?= _('register_dog') ?></h2>

    <label for="dog_name"><?= _('register_dog_name_label'); ?></label>
    <input type="text" name="dog_name" id="dog_name" value="<?= $data->dog_name ?>"><br>
    <label for="breed"><?= _('register_breed_label'); ?></label>
    <select name="breed" id="breed">
	<option value=""></option>
	<option value="1">vizsla</option>
	<option value="3">csivava</option>
    </select><br>
    <label for="breed_comment"><?= _('register_breed_comment_label'); ?></label>
    <input type="text" name="breed_comment" id="breed_comment" value="<?= $data->breed_comment ?>"><br>
    <label for="pedname"><?= _('register_pedname_label'); ?></label>
    <input type="text" name="pedname" id="pedname" value="<?= $data->pedname ?>"><br>
    <label for="pedno"><?= _('register_pedno_label'); ?></label>
    <input type="text" name="pedno" id="pedno" value="<?= $data->pedno ?>"><br>
    <input type="radio" name="dog_gender" value="M" id="dog_gender_male" <?= ($data->dog_gender == 'M') ? 'checked' : '' ?>>
    <label for="dog_gender_male"><?= _('dog_gender_male'); ?></label>
    <input type="radio" name="dog_gender" value="F" id="dog_gender_female" <?= ($data->dog_gender == 'F') ? 'checked' : '' ?>>
    <label for="dog_gender_female"><?= _('dog_gender_female'); ?></label><br>
    <label for="neutered"><?= _('register_neutered_label'); ?></label>
    <input type="checkbox" name="neutered" id="neutered" value="true" <?= ($data->neutered == 'true') ? 'checked' : '' ?>><br>
    <label for="neutreason"><?= _('register_neutreason_label'); ?></label>
    <input type="text" name="neutreason" id="neutreason" value="<?= $data->neutreason ?>"><br>
    <label for="neut_year"><?= _('register_neutdate_label'); ?></label>
    <input type="number" name="neut_year" id="neut_year" value="<?= $data->neut_year ?>">
    <input type="number" name="neut_month" id="neut_month" value="<?= $data->neut_month ?>">
    <input type="number" name="neut_day" id="neut_day" value="<?= $data->neut_day ?>"><br>
    <label for="famagey"><?= _('register_famage_label'); ?></label>
    <input type="number" name="famagey" id="famagey" value="<?= $data->famagey ?>">
    <input type="number" name="famagem" id="famagem" value="<?= $data->famagem ?>">
    <input type="number" name="famaged" id="famaged" value="<?= $data->famaged ?>"><br>
    <label for="origin"><?= _('register_origin_label'); ?></label>
    <input type="text" name="origin" id="origin" value="<?= $data->origin ?>"><br>
    <label for="role"><?= _('register_role_label'); ?></label>
    <input type="text" name="role" id="role" value="<?= $data->role ?>"><br>
    <label for="motive_food"><?= _('register_motive_food_label'); ?></label>
    <input type="text" name="motive_food" id="motive_food" value="<?= $data->motive_food ?>"><br>
    <label for="motive_toy"><?= _('register_motive_toy_label'); ?></label>
    <input type="text" name="motive_toy" id="motive_toy" value="<?= $data->motive_toy ?>"><br>
    <label for="train"><?= _('register_train_label'); ?></label>
    <input type="text" name="train" id="train" value="<?= $data->train ?>"><br>
    <label for="trexam"><?= _('register_trexam_label'); ?></label>
    <input type="text" name="trexam" id="trexam" value="<?= $data->trexam ?>"><br>
    <label for="behproblem"><?= _('register_behproblem_label'); ?></label>
    <input type="text" name="behproblem" id="behproblem" value="<?= $data->behproblem ?>"><br>
    <label for="dog_comment"><?= _('register_dog_comment_label'); ?></label>
    <textarea name="dog_comment" id="dog_comment"><?= ($data->dog_comment != '') ? $data->dog_comment : _('register_dog_comment_text'); ?></textarea><br>

    <input type="submit" value="<?= _('register_button_text'); ?>" name="submitRegister">
</form>
<?php if ($data->isError()) : ?>
    <p class="error_msg"><?php echo $data->error; ?></p>
<?php endif; ?>
