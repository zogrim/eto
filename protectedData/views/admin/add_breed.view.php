<form action="" method="post">
    <h2><?= _('admin_add_breed_header') ?></h2>
    <label for="name_id"><?= _('breed_name_id_label') ?></label>
    <input type="text" name="name_id" id="name_id" value="<?= $data->name_id ?>"><br>
    <?php foreach($data->locales as $loc => $text) : ?>
	<label for="<?= $loc ?>_text"><?= $text . ' ' ?></label>
	<label for="<?= $loc ?>_text"><?= _('breed_text_label') ?></label>
	<input type="text" name="<?= $loc ?>_text" id="<?= $loc ?>_text" value="<?php $key = $loc . '_text'; echo $data->$key?>"><br>
    <?php endforeach ?>
    <input type="submit" value=<?= _('add_breeed_button_text') ?> name="submitAddBreed"><br>
</form>
<?php if ($data->isError()) : ?>
    <p class="error_msg"><?php echo $data->error; ?></p>
<?php endif; ?>
