<!DOCTYPE html>

<html>
    <head>
	<meta charset="UTF-8">
	<title><?=_("site_title");?></title>
	<link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
	<div class="nav">
	    <nav>
		<a href="http://localhost/eto/etho-dogdatabase.elte.hu/">Index</a>
		<a href="http://localhost/eto/etho-dogdatabase.elte.hu/admin.php">Admin</a>
		<a href="http://localhost/eto/etho-dogdatabase.elte.hu/logout.php">Logout</a>
	    </nav>
	</div>
	<div class="container">
	    <?php include($path); ?>
	</div>
    </body>
</html>
