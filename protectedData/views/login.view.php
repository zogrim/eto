<form action="" method="post">
    <label for="email"><?= _('login_email_label'); ?></label>
    <input type="text" name="email" id="email" value="<?= $data->email?>"><br>
    <input type="submit" value=<?= _('login_button_text') ?> name="submitLogin">
    <input type="submit" value=<?= _('register_button_text') ?> name="submitToRegister" formaction="register.php">
</form>
<?php if ($data->isError()) : ?>
    <p class="error_msg"><?php echo $data->error; ?></p>
<?php endif; ?>
