<form action="" method="post">
    <label for="email"><?= _('login_email_label'); ?></label>
    <input type="text" name="email" id="email"
	   value="<?= $data->email ?>"><br>
    <label for="password"><?= _('login_password_label'); ?></label>
    <input type="password" name="password" id="password" autofocus><br>
    <input type="submit" value="<?= _('login_button_text'); ?>" name="submitAuth">
</form>
<?php if ($data->isError()) : ?>
    <p class="error_msg"><?php echo $data->error; ?></p>
<?php endif; ?>
    
