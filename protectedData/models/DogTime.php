<?php

class DogTime extends ActiveRecord\Model
{
    static $table_name = 'dog_times';
    
    static $belongs_to = array(
	array('dog'),
	array('creator',
	      'class_name' => 'User',
	      'foreign_key' => 'create_id',
	      'readonly' => true)
    );
    
}

?>
