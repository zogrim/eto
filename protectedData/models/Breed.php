<?php

class Breed extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('creator', 'class_name' => 'User',
	      'primary_key' => 'create_id',
	      'foreign_key' => 'id',
	      'readonly' => true),
	array('modifier',
	      'class_name' => 'User',
	      'primary_key' => 'modify_id',
	      'foreign_key' => 'id',
	      'readonly' => true)
    );

    static $has_many = array(
	array('breed_texts'),
	array('dogs')
    );

    static $validates_uniqueness_of = array(
	array('name_id', 'message' => 'breed_name_id_not_unique')
    );
    
}

?>
