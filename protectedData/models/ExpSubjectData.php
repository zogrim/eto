<?php

class ExpSubjectData extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('subject',
	      'class_name' => 'ExpSubject',
	      'foreign_key' => 'exp_subject_id'),
	array('create_id',
	      'class_name' => 'User')
    );
}

?>
