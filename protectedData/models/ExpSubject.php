<?php

class ExpSubject extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('dog'),
	array('experiment',
	      'foreign_key' => 'exp_id'),
	array('creator',
	      'class_name' => 'User',
	      'primary_key' => 'id',
	      'foreign_key' => 'create_id',
	      'readonly' => true)
    );

    static $has_many = array(
	array('data',
	      'class_name' => 'ExpSubjectData',
	      'foreign_key' => 'exp_subject_id')
    );
}

?>
