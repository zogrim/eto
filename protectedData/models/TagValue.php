<?php

class TagValue extends ActiveRecord\Model
{
    static $has_one = array(
	array('exp_tag',
	      'class_name' => 'ExpTag',
	      'foreign_key' => 'value_id'),
	array('dog_tag',
	      'class_name' => 'DogTag',
	      'foreign_key' => 'value_id'),
    );
}

?>
