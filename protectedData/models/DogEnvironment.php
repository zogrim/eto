<?php

class DogEnvironment extends ActiveRecord\Model
{
    static $table_name = 'dog_environments';
    
    static $belongs_to = array(
	array('dog'),
	array('creator', 'class_name' => 'User',
	      'foreign_key' => 'create_id',
	      'readonly' => true)
    );
}

?>
