<?php

class Dog extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('breed',
	      'readonly' => true),
	array('creator',
	      'class_name' => 'User',
	      'foreign_key' => 'create_id',
	      'readonly' => true),
	array('modifier',
	      'class_name' => 'User',
	      'foreign_key' => 'modify_id',
	      'readonly' => true)
    );
    
    static $has_many = array(
	array('ownerships'),
	array('times',
	      'class_name' => 'DogTime'),
	array('environments',
	      'class_name' => 'DogEnvironment'),
	array('tags',
	      'class_name' => 'DogTag'),
	array('subjects',
	      'class_name' => 'ExpSubject')
    );

    public function get_owner() {
	return isset($this->ownerships[0])
	    ? $this->ownerships[0]->owner
	     : NULL;
    }

    public function get_owners() {
	$owners = array();
	foreach($this->ownerships as $o)
	{
	    $owners[] = $o;
	}
	return $owners;
    }
}

?>
