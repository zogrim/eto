<?php

class ExpConnect extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('user'),
	array('experiment',
	      'foreign_key' => 'exp_id'),
	array('creator',
	      'class_name' => 'User',
	      'primary_key' => 'id',
	      'foreign_key' => 'create_id',
	      'readonly' => true)
    );
}

?>
