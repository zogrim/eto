<?php

class Ownership extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('dog'),
	array('owner',
	      'class_name' => 'User',
	      'primary_key' => 'id',
	      'foreign_key' => 'owner_id'),
	array('creator',
	      'class_name' => 'User',
	      'primary_key' => 'id',
	      'foreign_key' => 'create_id',
	      'readonly' => true)
    );
}

?>
