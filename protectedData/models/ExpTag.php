<?php

class ExpTag extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('experiment',
	      'foreign_key' = 'exp_id'),
	array('tag'),
	array('tag_value'),
	array('creator',
	      'class_name' => 'User',
	      'foreign_key' => 'create_id',
	      'readonly' => true),
	array('modifier',
	      'class_name' => 'User',
	      'foreign_key' => 'modify_id',
	      'readonly' => true)
    );
}

?>
