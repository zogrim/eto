<?php

class Motd extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('creator',
	      'class_name' => 'User',
	      'foreign_key' => 'create_id',
	      'readonly' => true),
	array('modifier',
	      'class_name' => 'User',
	      'foreign_key' => 'modify_id',
	      'readonly' => true)
    );
}

?>
