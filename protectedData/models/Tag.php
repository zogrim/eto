<?php

class Tag extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('create_id', 'class_name' => 'User'),
	array('modify_id', 'class_name' => 'User')
    );

    static $has_many = array(
	array('exp_tags',
	      'class_name' => 'ExpTag'),
	array('dog_tags',
	      'class_name' => 'DogTag'),
	array('tag_texts',
	      'class_name' => 'TagText')
    );
}

?>
