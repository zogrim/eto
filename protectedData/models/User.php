<?php

class User extends ActiveRecord\Model
{
    static $belongs_to = array(
	array('create_id', 'class_name' => 'User'),
	array('modify_id', 'class_name' => 'User')
    );

    static $has_many = array(
	array('ownerships',
	      'foreign_key' => 'owner_id'),
	array('exp_connects',
	      'class_name' => 'ExpConnect'),
	array('experiments',
	      'foreign_key' => 'owner_id'),
	array('motds',
	      'foreign_key' => 'create_id'),
	array('dogs',
	      'through' => 'ownerships',
	      'foreign_key' => 'owner_id')
    );
    
    static $validates_presence_of = array(
	array('email', 'message' => 'ardb_user_email_required'),
	array('name', 'message' => 'ardb_user_name_required'),
	array('first_time', 'message' => 'ardb_user_internal_error'),
	array('active', 'message' => 'ardb_user_internal_error'),
	array('type', 'message' => 'ardb_user_internal_error'),
	array('gender', 'message' => 'ardb_user_gender_required')
    );
    
    public function set_password($pw) {
	// TODO write actual password hashing
	$this->assign_attribute('password', $pw);
    }

    public $birth_year;
    public $birth_month;
    public $birth_day;
}

?>
