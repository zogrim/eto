<?php

class Experiment extends ActiveRecord\Model
{
    static $belongs_to = array(

	array('owner',
	      'class_name' => 'User',
	      'foreign_key' => 'owner_id',
	      'readonly' => true),
	array('modifier',
	      'class_name' => 'User',
	      'foreign_key' => 'modify_id',
	      'readonly' => true),
	array('selector')
    );

    static $has_many = array(
	array('connects',
	      'class_name' => 'ExpConnect',
	      'foreign_key' => 'exp_id'),
	array('subjects',
	      'class_name' => 'ExpSubject',
	      'foreign_key' => 'exp_id'),
	array('texts',
	      'class_name' => 'ExpText',
	      'foreign_key' => 'exp_id'),
	array('exp_tags',
	      'class_name' => 'ExpTag',
	      'foreign_key' => 'exp_id'),
	
    );
}

?>
