<?php

class UIData implements IteratorAggregate {
    
    private $data;

    private $isError;

    public function __construct($d = array())
    {
	$this->data = array();
	foreach($d as $k => $v)
	{
	    $this->data[$k] = $v;
	}
	$this->isError = false;
    }

    public function __get($key)
    {
	if (isset($this->data[$key]))
	{
	    return $this->data[$key];
	}
	return NULL;
    }
    
    public function __set($key, $value)
    {
	$this->data[$key] = $value;
    }

    public function __isset($key)
    {
	return (isset($this->data[$key]));
    }

    public function getIterator()
    {
	return new ArrayIterator($this->data);
    }

    public function resetError()
    {
	$this->isError = false;
    }
    
    public function isError()
    {
	return $this->isError;
    }
    
    public function error($msg)
    {
	$this->isError = true;
	$this->data['error'] = $msg;
    }

    public function errorFromModel($errors)
    {
	$this->isError = true;
	$msgs = '';
	foreach($errors as $key => $value)
	{
	    foreach($value as $k => $msg)
	    {
		$msgs .= $msg . '<br>';
	    }
	}
	$this->data['error'] = $msgs;
    }
    
    // check presence of $key member in $post
    // if previous error message was not stored
    // store new one
    public function checkPresence($post, $key, $msg)
    {
	if (!$this->isError && (!isset($post->$key) || $post->$key === ''))
	{
	    $this->error($msg);
	}
    }

    public function copyData($d)
    {
	foreach($d as $key => $value)
	{
	    $this->data[$key] = $value;
	}
    }
}

?>
