<?php

class Get {
    private static $instance;

    private function __construct() {}

    public static function getInstance()
    {
	if (!isset(self::$instance))
	{
	    self::$instance = new self;
	}
	return self::$instance;
    }

    public function __get($key)
    {
	if (isset($_GET[$key]))
	{
	    return htmlspecialchars($_GET[$key]);
	}
	return NULL;
    }
    
    public function __isset($key)
    {
	return (isset($_GET[$key]));
    }

    public function isRequest()
    {
	return ($_SERVER['REQUEST_METHOD'] === 'GET');
    }
}

?>
