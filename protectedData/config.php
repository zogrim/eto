<?php

define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('SITE_ROOT', DOC_ROOT . 'eto/etho-dogdatabase.elte.hu/');
define('PROT_ROOT', DOC_ROOT . 'eto/protectedData/');

$localizations = array('en' => _('config_localization_english'),
		       'hu' => _('config_localization_hungarian'));
?>
